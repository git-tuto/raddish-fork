#include "craft.h"

Craft::Craft(QWidget* parent): QWidget(parent),
    witch_{},
    scroll_{ new Scroll{ witch_, this } },
    toolBar_{ new QToolBar{} },
    strokeColor_{ Qt::red }
{
    QVBoxLayout* layin{ new QVBoxLayout{} };
    layin->addWidget(scroll_);
    layin->addWidget(toolBar_);
    setLayout(layin);

    QTimer::singleShot(0, this, SLOT(createPresetButton()));
}

void Craft::setRune(Rune r)
{
    witch_.setRune(r);
    witch_.burn();

    scroll_->refresh();
}

void Craft::createPresetButton()
{
    PresetButton* presetButton{ new PresetButton{ strokeColor_ } };
    toolBar_->addWidget(presetButton);

    connect(presetButton, SIGNAL(picked(Rune)), this, SLOT(setRune(Rune)));
}
