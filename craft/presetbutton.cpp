#include "icons.h"
#include "witch.h"
#include <QMenu>

#include "presetbutton.h"

PresetButton::PresetButton(const QColor& color, QWidget* parent): QToolButton(parent),
    strokeColor_{ color }
{
    QMenu* presetMenu{ new QMenu{ "Presets" } };

    for (int p{0}; p < NUM_PRESETS; ++p)
    {
        Preset preset{ static_cast<Preset>(p) };
        QString name{};
        switch (preset) {
        default:
        case LINE:       name = "Line"; break;
        case SEMICIRCLE: name = "Semicircle"; break;
        case CORNER:     name = "Corner"; break;
        case RANDOM:     name = "Random"; break;
        }

        QAction* a{ presetMenu->addAction(Icon::preset(preset, strokeColor_), name) };

        a->setProperty("r", p);

        if (p == 0)
            setDefaultAction(a);
    }

    connect(presetMenu, SIGNAL(triggered(QAction*)), this, SLOT(pickRune(QAction*)));
    setMenu(presetMenu);
}

void PresetButton::pickRune(QAction* a)
{
    setDefaultAction(a);

    Preset preset{ static_cast<Preset>(
                    a->property("r").toInt()) };
    Rune r{};
    switch (preset) {
    default:
    case LINE:       r = Rune::line(); break;
    case SEMICIRCLE: r = Rune::semicircle(); break;
    case CORNER:     r = Rune::corner(); break;
    case RANDOM:     r = Rune::vary(); break;
    }

    emit picked(r);
}

QIcon PresetButton::icon(Preset preset)
{

}
