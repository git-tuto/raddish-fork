#ifndef HATCH_H
#define HATCH_H

#include <Witch>

#include <QPixmap>

class Hatch: public QPixmap
{
public:
    Hatch(const Witch& witch);
    Hatch(Witch* witch = nullptr);

    void update();

private:
    Witch* ed_;
    Witch witch_;
};

#endif // HATCH_H
