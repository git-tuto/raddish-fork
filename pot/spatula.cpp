#include <QPainter>

#include "spatula.h"

Spatula::Spatula(): QObject(),
    pixmap_{ 2 * UNIT + 1, 2 * UNIT + 1 },
    pos_{},
    angle_{},
    drag_{ false },
    button_{ Qt::NoButton }
{
    update();
}

void Spatula::setPos(const QPoint& pos, Qt::MouseButton button)
{
    bool wasDragOn{ drag_ };

    if (button_ == Qt::NoButton)
        button_ = button;

    if (pos_ == pos)
    {
        drag_ &= button != Qt::NoButton;
    }
    else
    {
        QLineF delta{ pos_, pos };
        float targetAngle{ delta.angle() };
        float t{ M_SQRT1_2  * delta.length()
              / (delta.p1() - delta.p2()).manhattanLength()  };
        float a{ lerpDeg(angle(), targetAngle, t) };

        pos_  = pos;
        setAngle(a);

        drag_ = button != Qt::NoButton;
    }

    if (wasDragOn != drag_)
    {
        if (drag_)
            emit dragOn(button_);
        else
            emit dragOff(button_);
    }

    if (button == Qt::NoButton)
        button_ = Qt::NoButton;
}

void Spatula::update()
{
    pixmap_.fill(Qt::transparent);
    QPainter p{ &pixmap_ };

    int hMid{ width()  / 2 };
    int vMid{ height() / 2 };
    QPoint from{ -hMid, 0 };
    QPoint   to{  hMid, 0 };
    QMatrix m{};
    m.translate(hMid, vMid);
    m.rotate(-angle_ + 90.0f);
    from = m.map(from);
    to = m.map(to);

    p.setPen(Qt::yellow);
    p.drawLine(from, to);
    p.setPen(Qt::black);
    p.drawPoint(hMid, vMid);
}
