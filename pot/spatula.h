#ifndef SPATULA_H
#define SPATULA_H

#include "luckeymath.h"

#include <QPixmap>

class Spatula: public QObject
{
    Q_OBJECT

public:
    Spatula();

    operator QPixmap() const { return pixmap_;          }
    int width()        const { return pixmap_.width();  }
    int height()       const { return pixmap_.height(); }

    QPoint pos() const { return pos_; }
    void setPos(const QPoint& pos, Qt::MouseButton button = Qt::NoButton);

    float angle() const { return angle_; }
    void setAngle(float a)
    {
        if (angle_ == a)
            return;

        angle_ = a;
        update();
    }

    QRect rect() const
    {
        return { pos_ - QPoint{ width() / 2, height() / 2 },
                 QSize{ width(), height() } };
    }

    void leave() { setPos(QPoint{ INT_MAX, INT_MAX }); }
    bool out() { return pos_.x() == INT_MAX
                     || pos_.y() == INT_MAX; }
    bool dragged() const { return drag_; }
    Qt::MouseButton button() const { return button_; }

signals:
    void dragOn (Qt::MouseButton b);
    void dragOff(Qt::MouseButton b);

private:
    void update();

    QPixmap pixmap_;
    QPoint pos_;
    float angle_;
    bool drag_;
    Qt::MouseButton button_;
};

#endif // SPATULA_H
