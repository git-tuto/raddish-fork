#include "icons.h"

#include "instrument.h"
#include "movein.h"
#include "drawin.h"

Instrument* Instrument::held_{ nullptr };
std::unordered_map<Tool, Instrument*> Instrument::instrumentarium_{};

Instrument::Instrument(Tool t):
    type_{ t },
    action_{ new QAction{ this } }
{
    action_->setText(*this);
    action_->setIcon(Icon::tool(*this, false));
    action_->setCheckable(true);
}

Instrument* Instrument::grab(Tool t, bool hold)
{
    if (instrumentarium_.find(t) == instrumentarium_.end())
    {
        switch (t) {
        case MOVE: return instrumentarium_[t] = new MoveIn{};
        case DRAW: return instrumentarium_[t] = new DrawIn{};
        default: return nullptr;
        }
    }

    if (hold)
    {
        held_ = instrumentarium_[t];
        QAction* h{ *held_ };
        h->setIcon(Icon::tool(*held_, true));
        h->setChecked(true);

        for (Instrument* i: Instrument::all())
        {
            if (i != held_)
            {
                QAction* a{ *i };
                a->setChecked(false);
                a->setIcon(Icon::tool(*i, false));
            }
        }
    }

    return instrumentarium_[t];
}

Instrument* Instrument::grab(QObject* action)
{
    for (Instrument* i: all())
        if (action == i->action())
            return grab(i->type_);

    return nullptr;
}

std::vector<Instrument*> Instrument::all()
{
    std::vector<Instrument*> instruments{};

    for (auto i: instrumentarium_)
        instruments.push_back(i.second);

    return instruments;
}

std::vector<QAction*> Instrument::actions(Tool t)
{
    std::vector<QAction*> instruments{};

    if (t == ZEBRA)
        for (auto i: instrumentarium_)
            instruments.push_back(i.second->action());
    else
        grab(t, false)->operations();

    return instruments;
}
