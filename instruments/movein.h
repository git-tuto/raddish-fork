#ifndef MOVEIN_H
#define MOVEIN_H

#include "instrument.h"

class MoveIn: public Instrument
{
public:
    MoveIn(): Instrument(MOVE) {}

    void strum(const QLineF& at) override;
};

#endif // MOVEIN_H
