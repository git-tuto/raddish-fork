#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "instrument.h"
#include "activecraft.h"
#include "pot.h"

#include <QDesktopWidget>
#include <QDockWidget>

#include <QMainWindow>

class MainWindow: public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);

private:
    void createCrafts();

    QDockWidget* createDockWidget(QWidget* w, Qt::DockWidgetArea area)
    {
        QDockWidget* dockWidget{ new QDockWidget(w->objectName(), this) };
        dockWidget->setWidget(w);
        dockWidget->setObjectName(w->objectName() + "Dock");
        w->setParent(dockWidget);

        addDockWidget(area, dockWidget);

        return dockWidget;
    }

    void createToolBar();
    QAction* createTool(Tool t);

    std::vector<Craft*> crafts_;
    Pot* pot_;

private slots:
    void toolTriggered();
};
#endif // MAINWINDOW_H
