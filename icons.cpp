#include "icons.h"

Runicon Icon::runes_{};

QIcon Icon::preset(Preset preset, const QColor& col)
{
    QImage image{ emptyIcon() };

    QPainter p{ &image };
    p.setRenderHint(QPainter::Antialiasing);

    float c{ image.width() / 2 - 1.5 };
    p.setBrush(Qt::black);
    p.setOpacity(0.4f);
    p.drawEllipse(1, 1, 2 * c, 2 * c);
    p.setPen({ Qt::gray, 2.0 });
    p.setBrush(Qt::NoBrush);
    p.setOpacity(0.6);
    p.drawEllipse(1, 1, 2 * c, 2 * c);

    Rune r{ -1, 1, 0, 0};
    switch (preset) {
    default:
    case LINE:       r = Rune::line(); break;
    case SEMICIRCLE: r = Rune::semicircle(); break;
    case CORNER:     r = Rune::corner(); break;
    case RANDOM:     break;
    }
    Broom b{ { 2, c + 1 },
             { image.width() - 2, c + 1 } };

    p.setOpacity(1);
    p.setPen({ col, 4.0 });
    p.setBrush(Qt::NoBrush);

    p.drawPath(Witch{{b}, {r}});
    if (r.rin() == -1)
        p.drawPath(Witch{{b}, {{r.inverted()}}});

    p.end();

    return { QPixmap::fromImage(image) };
}

QIcon Icon::tool(Tool tool, bool active)
{
    QImage image{ emptyIcon() };

    QPainter p{ &image };
    p.setRenderHint(QPainter::Antialiasing);

    for (int i{0}; i <= active; ++i)
    {
        p.setPen({ QColor{255, 255, 128 * i}, 2 + 2 * active - i, Qt::SolidLine, Qt::FlatCap });

        if (tool == MOVE)
        {
            QPoint c{ image.rect().center()};

            p.drawLine(QLineF{{ 3, c.y() - 0.25},
                              { image.width() - 4, c.y() - 0.25 }});

            p.drawLine(QLineF{{ c.x() - 0.25, 3},
                              { c.x() - 0.25, image.height() - 4 }});

            for (int w{0}; w < 4; ++w)
            {
                int o{ image.width() / 6 - 1 };
                int x1{ -o };
                int x2{ +o };
                QPoint a{ x1, -c.y() / 2 - 3 };
                QPoint b{ x2, -c.y() / 2 - 3 };
                QMatrix m{};
                m.translate(c.x(), c.y());
                m.rotate(90 * w);
                a = m.map(a);
                b = m.map(b);

                p.drawPath(Witch{{ a, b }, { Rune::corner() }});
            }
        }
        else if (tool == DRAW)
        {
            int o{ image.width() / 8 - 1 };
            QPoint from{ o, 3 };
            QPoint to{ image.width()  - o, image.height() - 4 };
            p.drawPath(Witch{{from, to},
                             { Rune::beauty().reversed() }});
            ;
        }
    }

    return { QPixmap::fromImage(image) };
}

QIcon Icon::rune(Rune r)
{
    return { QPixmap::fromImage(emptyIcon()) };
}
